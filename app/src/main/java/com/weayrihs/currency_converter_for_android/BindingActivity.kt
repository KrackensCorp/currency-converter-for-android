package com.weayrihs.currency_converter_for_android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BindingActivity<VB : ViewBinding> : AppCompatActivity() {

	private var _binding: VB? = null
	val binding
		get() = _binding ?: throw NullPointerException("Binding can't be null")

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		_binding = binding()
		setContentView(binding.root)
	}

	protected abstract fun binding(): VB

	override fun onDestroy() {
		super.onDestroy()
		_binding = null
	}
}