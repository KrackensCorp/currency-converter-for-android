package com.weayrihs.currency_converter_for_android

import com.weayrihs.currency_converter_for_android.databinding.ActivityMainBinding

class MainActivity : BindingActivity<ActivityMainBinding>() {

	override fun binding(): ActivityMainBinding =
		ActivityMainBinding.inflate(layoutInflater)

}