package com.weayrihs.currency_converter_for_android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weayrihs.currency_converter_for_android.databinding.FragmentMainBinding

class MainFragment : BindingFragment<FragmentMainBinding>() {

	override fun binding(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): FragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		content()
	}

	private fun content() {
		//TODO
	}
}